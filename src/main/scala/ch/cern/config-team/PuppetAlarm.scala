package ch.cern.config

import ch.cern.monitoring.config.Config.parseConfig
import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.json4s.CustomSerializer
import org.json4s.JsonAST.JValue
import org.json4s.JsonDSL._
import org.json4s._
import org.json4s.native.Serialization._
import scalaj.http.Http
import org.apache.spark.sql.functions.exp





case class PuppetAlarm(
                       source : String = "collectd",
                       entities : String,
                       status : String = "FAILURE",
                       alarm_name : String = "puppet_unclean",
                       functional_element: String = "cfg" ,
                       message : String = "Not a perfect puppet run for over 68 hours",
                       targets : String = "Grafana") {}



class PuppetAlarmSerializer extends CustomSerializer[PuppetAlarm] (format => ( {

   case json: JValue =>
   implicit val formats = format
   val source = (json \ "source").extract[String]
   val status = (json \ "status").extract[String]
   val functional_element = (json \ "functional_element").extract[String]
   val entities = (json \ "entities").extract[String]
   val alarm_name = (json \ "alarm_name").extract[String]
   val message = (json \ "message").extract[String]
   val targets = (json \ "targets").extract[String]


PuppetAlarm(source, entities, alarm_name, message, targets, status, functional_element) }, {
    case puppetAlarm: PuppetAlarm =>
    implicit val formats = format
    ("source" -> puppetAlarm.source) ~
    ("functional_element" -> puppetAlarm.functional_element) ~
    ("status" -> puppetAlarm.status) ~
    ("entities" -> puppetAlarm.entities) ~
    ("alarm_name" -> puppetAlarm.alarm_name) ~
    ("message" -> puppetAlarm.message) ~
    ("targets" -> puppetAlarm.targets)

}))





object PuppetAlarmApplication {

  def main(args: Array[String]) {

    val APPLICATION_NAME = "Spark Puppet Alarms"
    val config = parseConfig(args, APPLICATION_NAME)


    var sparkConf = new SparkConf()

    val spark = SparkSession
      .builder
      .appName(APPLICATION_NAME)
      .config(sparkConf)
      .getOrCreate()

    spark.conf.set("spark.sql.streaming.stateStore.minDeltasForSnapshot", 2)
    spark.conf.set("spark.sql.streaming.minBatchesToRetain", 3)
    spark.conf.set("spark.sql.shuffle.partitions", 50)
    spark.conf.set("spark.sql.streaming.schemaInference", value = true)



    import spark.implicits._
    val puppetDataRDD =
      spark
      .read
      .format("kafka")
      .option("kafka.bootstrap.servers", config.kafkaInputBrokers)
      .option("subscribe", "collectd_raw_puppet")
      .option("startingOffsets", "earliest")
      .load()
      .select($"value")
      .select($"value".cast("string"))
      .map(row => row.getAs[String]("value"))



    implicit val formats = org.json4s.DefaultFormats + new PuppetAlarmSerializer




    val puppetAlarms =
      spark
      .read
      .json(puppetDataRDD)
      .where("data.type = 'puppet_run'")
      .select($"data.*", $"metadata.*")
      .filter($"timestamp" + 244800000 > System.currentTimeMillis())
      .where("value_instance= 'out_of_sync'")
      .groupBy("host")
        .agg(min("value").alias("perfect_run"))//.where("perfect_run != 0")
      .filter($"perfect_run" > 0)
      .map(row => PuppetAlarm(entities = row.getAs[String]("host")))
      .collect()

//puppetAlarms.show(300, false)


      puppetAlarms.foreach(activity => {
      System.out.println(write(activity))
      Http("http://monit-alarms.cern.ch:10011")
      .postData("[" + write(activity) + "]")
      .asString.code
       })


    }

}
