## Command

/usr/lib/spark2.3.0/bin/spark-submit --driver-class-path spark-puppet-alarms-assembly-1.0.jar --class ch.cern.config.PuppetAlarmApplication --packages org.apache.spark:spark-sql-kafka-0-10_2.11:2.3.0 spark-puppet-alarms-assembly-1.0.jar



/usr/lib/spark2.3.0/bin/spark-submit --driver-class-path spark-puppet-alarms-assembly-1.0.jar --class ch.cern.config.PuppetAlarmApplication --packages org.apache.spark:spark-sql-kafka-0-10_2.11:2.3.0 spark-puppet-alarms-assembly-1.0.jar


About this job
======

This is a spark batch job that  is going through Kafka and gets all the nodes that did not hava a clean puppet run for over 2.8 days
After that it produces a GNI alarm
This runs in Chronos 1 per hour and the results are visible in Kibana 


----------------------------------------------------------------------------------------------------------------------------------------


Code Documentation
======

**puppetDataRDD**
 * creates json for all the needed fields
 * reads from kafka from the given endpoint that can be found on config.scala on kafka read
 * reads from collectd_raw_puppet plugin


**puppetAlarms**
 * reads from the RDD that is above and filters it from 'puppet_run' data.type
 * gets all the data and metadata about the puppet_run
 * filters so it can get the ones that are NOW-2.8 days before
 * groups by host and das an aggrigation
 * the aggrigation gets the minimum value of type 'out_of_sync' . If the minimum value is 0 that means that it had at least 1 perfect run for these 2.8 days
 * if it doesnt that means that we take that host as a problem


**puppetAlarms.foreach**
 * sends the GNI to the given endpoint

